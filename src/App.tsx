import React, { FC, useEffect, useState } from "react";

import getUsers from "./apis/apis";
import Header from "./components/Header/Header";
import { IUserCardProps } from "./components/UserCard/IUserCardProps";
import UsersListWidget from "./components/UsersListWidget/UsersListWidget";

const App: FC = () => {
  const [users, setUsers] = useState<IUserCardProps[]>([]);

  useEffect(() => {
    getUsers().then((data) => setUsers(data));
  }, []);

  return (
    <div>
      <Header />
      <UsersListWidget title="Users" users={users} />
    </div>
  );
};

export default App;
