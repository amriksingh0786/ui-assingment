import React from "react";
import { shallow } from "enzyme";

import Header from "./Header";

describe("Header Component", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Header />);
  });

  afterEach(() => {
    wrapper.unmount();
  });

  it("Header Component is rendering without crashes", () => {
    expect(wrapper).toBeTruthy();
  });

  it("Logo is rendering fine", () => {
    expect(wrapper.find(".logo").text()).toEqual("Logo");
  });
});
