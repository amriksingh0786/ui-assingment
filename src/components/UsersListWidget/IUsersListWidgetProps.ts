import { IUserCardProps } from "../UserCard/IUserCardProps";

export interface IUsersListWidgetProps {
  title: string;
  users: IUserCardProps[];
}
