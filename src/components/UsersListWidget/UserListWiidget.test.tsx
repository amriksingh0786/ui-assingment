import React from "react";
import { mount, shallow } from "enzyme";
import UsersListWidget from "./UsersListWidget";
import { IUsersListWidgetProps } from "./IUsersListWidgetProps";
import { IUserCardProps } from "../UserCard/IUserCardProps";

const data: IUsersListWidgetProps = {
  title: "Users",
  users: [
    {
      id: 1,
      first_name: "Amrik",
      last_name: "Singh Khalsa",
      email: "singhamrikkhalsa@gmail.com",
      avatar: "https://reqres.in/img/faces/5-image.jpg",
    },
  ],
};

describe("UserListWidget Component", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow<typeof UsersListWidget>(<UsersListWidget {...data} />);
  });

  afterEach(() => {
    wrapper.unmount();
  });

  it("UserListWidget rendering without crashes", () => {
    expect(wrapper).toBeTruthy();
  });

  it("checking props working correctly", () => {
    wrapper = mount(<UsersListWidget {...data} />);
    expect(wrapper.props()?.title).toBeTruthy();
    wrapper.props()?.users.map((user: IUserCardProps) => {
      expect(user?.first_name).toBeTruthy();
      expect(user?.last_name).toBeTruthy();
      expect(user?.email).toBeTruthy();
      expect(user?.avatar).toBeTruthy();
      expect(user?.id).toBeTruthy();
      return true;
    });
  });
});
