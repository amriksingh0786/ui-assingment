import React, { FC } from "react";
import { IUserCardProps } from "../UserCard/IUserCardProps";
import UserCard from "../UserCard/UserCard";
import { IUsersListWidgetProps } from "./IUsersListWidgetProps";

import "./UsersListWidget.scss";

const UsersListWidget: FC<IUsersListWidgetProps> = (props) => {
  const { title, users } = props;
  return (
    <div className="container user-cards-container">
      <h3 className="user-section-title">{title}</h3>
      <div className="user-list-container">
        {users?.map((user: IUserCardProps) => {
          return <UserCard key={user?.id} {...user} />;
        })}
      </div>
    </div>
  );
};

export default UsersListWidget;
