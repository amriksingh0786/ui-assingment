import React, { FC } from "react";

import "./UserCard.scss";
import { IUserCardProps } from "./IUserCardProps";

const UserCard: FC<IUserCardProps> = (user) => {
  const { first_name, last_name, email, avatar } = user;
  return (
    <div className="user-card">
      <div className="user-avatar">
        <img src={avatar} alt={first_name} />
      </div>
      <div className="user-details">
        <h4 className="user-name">{`${`${first_name} ${last_name}`}`}</h4>
        <h6 className="user-email">{email}</h6>
      </div>
    </div>
  );
};

export default UserCard;
