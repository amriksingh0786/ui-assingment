import React, { ComponentProps } from "react";
import { Story } from "@storybook/react";

import UserCard from "./UserCard";

export default {
  title: "UserCard",
  component: UserCard,
  args: {
    id: 1,
    first_name: "George",
    last_name: "Bluth",
    email: "geo@gmail.com",
    avatar: "https://reqres.in/img/faces/1-image.jpg",
  },
};

const Template: Story<ComponentProps<typeof UserCard>> = (args) => (
  <UserCard {...args} />
);

export const PrimaryUserCard = Template.bind({});
PrimaryUserCard.args = {
  id: 1,
  first_name: "George",
  last_name: "Bluth",
  email: "geo@gmail.com",
  avatar: "https://reqres.in/img/faces/1-image.jpg",
};
