import React from "react";
import { mount, shallow } from "enzyme";

import UserCard from "./UserCard";

const user = {
  first_name: "Amrik",
  last_name: "Singh Khalsa",
  email: "singhamrikkhalsa@gmail.com",
  id: 1,
  avatar: "https://reqres.in/img/faces/5-image.jpg",
};

describe("UserCard Component", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallow(<UserCard {...user} />);
  });

  afterEach(() => {
    wrapper.unmount();
  });

  it("UserCard rendering without crashes", () => {
    expect(wrapper).toBeTruthy();
  });

  it("checking props working correctly", () => {
    wrapper = mount(<UserCard {...user} />);
    expect(wrapper.props().first_name).toEqual("Amrik");
    expect(wrapper.props().last_name).toEqual("Singh Khalsa");
    expect(wrapper.props().email).toEqual("singhamrikkhalsa@gmail.com");
    expect(wrapper.props().avatar).toEqual(
      "https://reqres.in/img/faces/5-image.jpg"
    );
    expect(wrapper.props().id).toEqual(1);
  });
});
